# elflibviewer

![Screenshot](https://bitbucket.org/mpyne/elflibviewer/downloads/elflibviewer-screenshot.png)

This Qt 5 program (as of v0.10) gives a graphical overview of the library
dependencies or a given ELF-format executable library or program.

It can also be used to quickly find any libraries that depend (directly or
indirectly) on a given library name (similar to ldd --tree)

# Installation

    $ git clone git://bitbucket.org/mpyne/elflibviewer.git elflibviewer
    $ mkdir build
    $ cd build
    $ export CMAKE_MODULE_PATH="$(qmake -query QT_INSTALL_PREFIX)"
    $ cmake -DCMAKE_INSTALL_PREFIX=/path/to/prefix ../elflibviewer
    $ make -j4 && make install # or sudo make install

(The `CMAKE_MODULE_PATH` stuff is simply to ensure CMake finds Qt5 if you have
Qt5 installed somewhere weird, it's not required if you've already setup CMake
property).

# Usage

    elflibviewer $(which program_to_analyze)

You can also use File -> Open from within the application if you'd prefer.

## Search

The program is incredibly basic but does have a search line... if you type in a
library name to highlight and hit Enter, then all libraries/executables that
depend on a library that has a name containing your search term will be
highlighted in red. This is true even if the dependency is only indirect.

This can be useful for identifying which library is actually causing your
application to (try to) link to a certain library.

# Dependencies

## Runtime

- Qt 5 (QtCore, QtGui, QtWidgets), at least Qt 5.7.
- The readelf executable (from GNU binutils), which must be in `PATH`.

## Buildtime

- Qt 5 development libraries (QtCore, QtGui), at least Qt 5.7
- CMake 3.0+

# Author

Michael Pyne <mpyne@kde.org>

# License

GPLv2+
