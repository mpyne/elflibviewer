<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>MainWindow</name>
    <message>
        <source>Clear the selected library name</source>
        <translation>Ripulisci ricerca</translation>
    </message>
    <message>
        <source>Type a library name in here to highlight libraries that use it</source>
        <translation>Inserisci qui il nome di una libreria da evidenziare tra quelle in lista</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>&amp;Apri...</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <source>&amp;About...</source>
        <translation>I&amp;nformazioni...</translation>
    </message>
    <message>
        <source>ELF Library Viewer</source>
        <translation></translation>
    </message>
    <message>
        <source>File</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindowImpl</name>
    <message>
        <source>Shared Object</source>
        <translation>Oggetto Condiviso</translation>
    </message>
    <message>
        <source>Resolved Path</source>
        <translation>Percorso Risolto</translation>
    </message>
    <message>
        <source>Open executable/library</source>
        <translation>Apri eseguibile/libreria</translation>
    </message>
    <message>
        <source>About elflibviewer</source>
        <translation>Informazioni su elflibviewer</translation>
    </message>
    <message>
        <source>elflibviewer is a program to display the required library dependencies of a program or shared library (in ELF format).  It requires the readelf tool.

Copyright © 2007, 2009, 2013, 2015, 2020 Michael Pyne and © 2020 Andrea Zanellato.

This program may be distributed under the terms of the GNU GPL v2 (or any later version).

The application icon is from the KDE 4 Oxygen icon set, distributed under the LGPL, version 2.

Version: %1</source>
        <translation>elflibviewer è un programma per visualizzare le librerie richieste come dipendenze di un programma o libreria condivisa (nel formato ELF).  Richiede readelf.

Copyright © 2007, 2009, 2013, 2015, 2020 Michael Pyne e © 2020 Andrea Zanellato.

Questo programma è distribuito sotto i termini della licenza GNU GPL v2 (o qualsiasi versione successiva).

L&apos;icona dell&apos;applicazione proviene dal set Oxygen di KDE 4, distribuito sotto licenza LGPL, versione 2.

Versione: %1</translation>
    </message>
</context>
</TS>
