/* Copyright © 2007, 2013 Michael Pyne <mpyne@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "MainWindowImpl.h"

#include <QtGui>
#include <QtCore>
#include <QTranslator>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    app.setApplicationName(QLatin1String("elflibviewer"));
    app.setOrganizationName(QLatin1String("Michael Pyne"));
    app.setApplicationVersion(QLatin1String(g_versionString));

    // Load locale translation file if any
    QTranslator translator;
    const QString appDataPath = QStandardPaths::locate(QStandardPaths::GenericDataLocation, "elflibviewer", QStandardPaths::LocateDirectory);
    qDebug() << "App data path" << appDataPath;
    if (translator.load(QLocale(), QLatin1String("elflibviewer"), QLatin1String("_"), appDataPath))
        app.installTranslator(&translator);

    MainWindowImpl mw(0);
    mw.show();

    QStringList arguments = app.arguments();
    if(arguments.count() > 1) {
        mw.openFile(arguments.last());
    }

    app.connect(&mw, SIGNAL(quit()), SLOT(quit()));

    return app.exec();
}

// vim: set ts=8 sw=4 et fileencoding=utf8:
