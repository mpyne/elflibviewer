/* Copyright © 2007, 2009, 2013 Michael Pyne <mpyne@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <QFileDialog>

#include "MainWindowImpl.h"

class MainWindowImpl::Private
{
    friend class MainWindowImpl;

    QStringList basePaths;
    QStringList envPaths;
    QTimer highlightLibrariesTimer;
};

// Needs to be defined, but
// needs to be defined after MainWindowImpl::Private
MainWindowImpl::~MainWindowImpl() = default;

MainWindowImpl::MainWindowImpl(QWidget *parent)
    : QMainWindow(parent)
    , m_ui(make_unique<Ui::MainWindow>())
    , m_model(make_unique<QStandardItemModel>(this))
    , d(make_unique<Private>())
{
    m_ui->setupUi(this);
    m_ui->libView->setModel(m_model.get());

    d->basePaths = getBasePaths();
    d->envPaths = getSystemEnvPaths();

    // Used to automatically highlight libraries if the user stops typing but hasn't
    // hit enter or return.
    d->highlightLibrariesTimer.setInterval(1000);
    d->highlightLibrariesTimer.setSingleShot(true);

    connect(&d->highlightLibrariesTimer, &QTimer::timeout,
            this, &MainWindowImpl::highlightMatchingLibraries);
}

void MainWindowImpl::openFile(const QString &fileName)
{
    if(fileName.isEmpty())
        return;

    m_model->clear();
    const QStringList list = {
        tr("Shared Object"),
        tr("Resolved Path")
    };
    m_model->setHorizontalHeaderLabels(list);

    const QFileInfo fi(fileName);

    const QList<QStandardItem *> items = {
        new QStandardItem(fi.fileName()),
        new QStandardItem(fileName)
    };

    m_model->invisibleRootItem()->appendRow(items);
    addFile(fileName, m_model->item(0));
    m_ui->libView->expand(m_model->indexFromItem(items[0]));
    m_ui->libView->resizeColumnToContents(0);

    setWindowFilePath(fileName);
}

void MainWindowImpl::on_actionOpen_triggered()
{
    const QString fileName = QFileDialog::getOpenFileName(this, tr("Open executable/library"));
    openFile(fileName);
}

void MainWindowImpl::on_actionQuit_triggered()
{
    emit quit();
}

QStringList MainWindowImpl::constructSearchPaths(const LibSearchInfo &searchInfo) const
{
    // Create search path
    QStringList paths;

    // If RPATH was set **and** RUNPATH was not, look in RPATH first.
    if(searchInfo.runPath.isEmpty() && !searchInfo.rPath.isEmpty())
        paths << searchInfo.rPath.split(":", QString::SkipEmptyParts);

    paths << d->envPaths;

    if(!searchInfo.runPath.isEmpty())
        paths << searchInfo.runPath.split(":", QString::SkipEmptyParts);

    paths << d->basePaths;

    // Assume that if these directories are present, that they're automatically
    // part of the ld.so search path
    if(QDir("/lib64").exists()) {
        paths << "/lib64";
    }

    if(QDir("/usr/lib64").exists()) {
        paths << "/usr/lib64";
    }

    paths << "/lib" << "/usr/lib";

    return paths;
}

void MainWindowImpl::addFile(const QString &fileName, QStandardItem *root)
{
    QList<QStandardItem *> items;
    const QFileInfo baseLibInfo(fileName);
    QProcess readelf;

    readelf.start("readelf", QStringList() << "-d" << fileName);
    if(!readelf.waitForFinished())
        return;

    QTextStream ts(&readelf);
    QRegExp soPat("\\[(.*):*\\]$");
    QRegExp originPat(
            "\\$" // Find Leading $
            "(?:" // Non-capture group start
            "\\{ORIGIN\\}|ORIGIN"
            ")\\b"      // End group, and at a word boundary
            );
    LibSearchInfo info;

    // List of libraries to search for.  We cannot search as soon as its
    // encountered because we must wait until we've seen rPath and runPath.
    QStringList libs;

    while(!ts.atEnd()) {
        const QString str = ts.readLine();

        if(str.contains("(RPATH)") && soPat.indexIn(str) != -1)
            info.rPath = soPat.cap(1).replace(originPat, baseLibInfo.canonicalPath());
        else if(str.contains("(RUNPATH)") && soPat.indexIn(str) != -1)
            info.runPath = soPat.cap(1).replace(originPat, baseLibInfo.canonicalPath());
        else if(str.contains("(NEEDED)") && soPat.indexIn(str) != -1)
            libs << soPat.cap(1);
    }

    for(const auto &soname : qAsConst(libs)) {
        bool hadLib = m_libs.contains(soname);
        const QString lib = resolveLibrary(soname, info);
        items << new QStandardItem(soname);

        if(lib.isEmpty()) {
            items << new QStandardItem("not found");
            qDebug() << "Unable to find" << soname;

            QFont f = items[0]->font();
            f.setItalic(true);

            items[0]->setFont(f);
            items[1]->setFont(f);
        }
        else
            items << new QStandardItem(lib);

        root->appendRow(items);

        if(!lib.isEmpty() && !hadLib)
            addFile(lib, root->child(root->rowCount() - 1, 0));

        items.clear();
    }
}

QString MainWindowImpl::resolveLibrary(const QString &library, const LibSearchInfo &info)
{
    if(m_libs.contains(library))
        return m_libs[library];

    const QStringList paths = constructSearchPaths(info);

    for(const auto &path : paths) {
        const QFileInfo qfi(path + QStringLiteral("/") + library);
        if(qfi.exists()) {
            // Resolve symlink.
            m_libs[library] = qfi.canonicalFilePath();
            return qfi.canonicalFilePath();
        }
    }

    qDebug() << "Failed to find" << library << "in" << paths;
    return QString();
}

void MainWindowImpl::resetItems(QStandardItem *root)
{
    for(int i = 0; i < root->rowCount(); ++i)
        resetItems(root->child(i, 0));

    root->setForeground(qGuiApp->palette().windowText());
}

void MainWindowImpl::on_actionAbout_triggered()
{
    QMessageBox::about(this, tr("About elflibviewer"),
        tr("elflibviewer is a program to display the required library dependencies of"
        " a program or shared library (in ELF format).  It requires the readelf"
        " tool.\n\nCopyright © 2007, 2009, 2013, 2015, 2020 Michael Pyne"
        " and © 2020 Andrea Zanellato.\n\nThis program may be distributed"
        " under the terms of the GNU GPL v2 (or any later version).\n\n"
        "The application icon is from the KDE 4 Oxygen icon set, distributed under the "
        "LGPL, version 2."
        "\n\nVersion: %1").arg(g_versionString));
}

void MainWindowImpl::highlightMatchingLibraries()
{
    const QString findText = m_ui->libSearchName->text();

    resetItems(m_model->invisibleRootItem());
    if(findText.isEmpty())
        return;

    const QList<QStandardItem *> results = m_model->findItems(findText, Qt::MatchContains | Qt::MatchRecursive);
    for(const auto &i : results) {
        i->setForeground(Qt::red);
        auto item = i->parent();
        while(item) {
            item->setForeground(Qt::red);
            item = item->parent();
        }
    }
}

void MainWindowImpl::restartTimer()
{
    d->highlightLibrariesTimer.start();
}

static QStringList getLinkerPathsFromFile(const QString &file)
{
    QFile linkerConfFile(file);

    if (!linkerConfFile.open(QIODevice::ReadOnly)) {
        return QStringList();
    }

    QTextStream in(&linkerConfFile);
    QStringList paths;

    while(!in.atEnd()) {
        QString libpath = in.readLine();

        // Remove comments
        libpath.replace(QRegExp("^\\s*#\\s*"), QString());
        if (libpath.contains(QRegExp("^\\s*$"))) {
            continue;
        }

        // Handle include entries, which permit wildcards.
        if (libpath.startsWith(QLatin1String("include "))) {
            QString libConfPaths = libpath;
            libConfPaths.replace(QLatin1String("include "), QString());

            const QFileInfo linkerConfFileInfo(file);
            QString nameFilter;
            int index = -1;

            if ((index = libConfPaths.lastIndexOf(QLatin1Char('*'))) != -1) {
                nameFilter = libConfPaths.mid(index);

                libConfPaths.truncate(index);
                libConfPaths.prepend(QLatin1Char('/'));
                libConfPaths.prepend(linkerConfFileInfo.absolutePath());
            }

            // Sort the returned files so that entries are read in desired
            // order.
            const QDir confDirectory(libConfPaths, nameFilter);
            QStringList includedConfFiles = confDirectory.entryList();
            includedConfFiles.sort();

            foreach (const auto &includedConfFile, qAsConst(includedConfFiles)) {
                paths += getLinkerPathsFromFile(
                        confDirectory.absoluteFilePath(includedConfFile));
            }

            return paths;
        }

        const QString realPath = QFileInfo(libpath).canonicalFilePath();

        if (!realPath.isEmpty()) {
            paths << realPath;
        }
    }

    return paths;
}

QStringList MainWindowImpl::getBasePaths() const
{
    // Open system file defining standard library paths.
    QStringList paths;

    paths = getLinkerPathsFromFile(QLatin1String("/etc/ld.so.conf"));
    paths.removeDuplicates();

    return paths;
}

QStringList MainWindowImpl::getSystemEnvPaths() const
{
    const auto env = QProcessEnvironment::systemEnvironment();

    const QString libPath = env.value(QLatin1String("LD_LIBRARY_PATH"));
    return libPath.isEmpty()
        ? QStringList()
        : libPath.split(QLatin1Char(':'));
}

// vim: set ts=8 sw=4 et fileencoding=utf8:
